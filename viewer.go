package main

import (
	"fmt"
	"fyne.io/fyne"
	fyneApp "fyne.io/fyne/app"
	"fyne.io/fyne/layout"
	"fyne.io/fyne/widget"
	"gitlab.com/cvut_magister/thesis/viewer/configuration"
	"gitlab.com/cvut_magister/thesis/viewer/contract"
	"gitlab.com/cvut_magister/thesis/viewer/icon"
	"gitlab.com/cvut_magister/thesis/viewer/light_bed"
	"gitlab.com/cvut_magister/thesis/viewer/position_resolver"
	"gitlab.com/cvut_magister/thesis/viewer/transport"
	"log"
	"os"
	"runtime"
	"syscall"
	"time"
)

func main() {
	config, err := configuration.Build("jan")
	if err != nil {
		panic(err)
	}

	var communication contract.Transport
	var positionResolver contract.PositionResolver

	switch config.Transport {
	case contract.FakeServer:
		communication = transport.NewFakeServer(config.TransportArgs.SendEveryDuration)

	case contract.RestoringServer:
		communication = transport.NewRestoringServer(
			config.TransportArgs.SendEveryDuration,
			*config.TransportArgs.StorePath,
			config.TransportArgs.StoreTag,
		)

	case contract.UdpServer:
		if config.TransportArgs.AnnounceDuration == nil {
			panic("Missing configuration 'AnnounceTimeout', cannot continue")
		}
		if config.TransportArgs.Port == nil {
			panic("Unknown server port, cannot continue")
		}

		communication = transport.NewUdpServer(
			*config.TransportArgs.Port,
			config.TransportArgs.Interfaces,
			*config.TransportArgs.AnnounceDuration,
			config.TransportArgs.AnnounceAddress,
			config.TransportArgs.AnnouncePort,
			config.TransportArgs.EnableZeroConfig,
		)

	default:
		panic(fmt.Sprintf("Unknown server %s", config.Transport))
	}

	switch config.PositionResolver {
	case contract.NaivePositionResolver:
		positionResolver = position_resolver.NewNaivePositionResolver(
			config.PositionResolverArgs.InterestCount,
			config.PositionResolverArgs.BankCount,
			config.PositionResolverArgs.OverdeterminedIteration,
			config.LightBedProperties,
		)
	case contract.StoringPositionResolver:
		positionResolver = position_resolver.NewStoringPositionResolver(
			config.PositionResolverArgs.StorePath,
			config.PositionResolverArgs.StoreTag,
		)

	default:
		panic(fmt.Sprintf("Unknown resolver %s", config.PositionResolver))
	}

	app := fyneApp.New()
	app.SetIcon(icon.LifeBitmap)

	w, _ := newWindow(app, communication, positionResolver, config)

	go memoryChecker(w, config) // fyne has memory leak problem, there is also no time to solve it, brute force solution, but it is solution

	w.Show()

	app.Run()

	<-time.After(100 * time.Millisecond) // give everyone time to gracefully end
}

func memoryChecker(w fyne.Window, config *configuration.Configuration) {
	tick := time.NewTicker(10 * time.Second)

	var m runtime.MemStats
	for {
		<-tick.C

		runtime.ReadMemStats(&m)

		allocatedMemory := bToMb(m.TotalAlloc)

		if config.AllowedMemoryUsage != nil && allocatedMemory > *config.AllowedMemoryUsage {
			log.Printf("Viewer allocated %vMb, restarting process...", allocatedMemory)

			w.Close()

			restart()
		} else {
			log.Printf("Viewer allocated %vMb", allocatedMemory)
		}
	}
}

func bToMb(b uint64) uint64 {
	return b / 1024 / 1024
}

func restart() {
	execSpec := &syscall.ProcAttr{
		Env:   os.Environ(),
		Files: []uintptr{os.Stdin.Fd(), os.Stdout.Fd(), os.Stderr.Fd()},
	}
	fork, err := syscall.ForkExec(os.Args[0], os.Args, execSpec)

	if err != nil {
		panic(err)
	}

	log.Printf("Started new fork: %d", fork)
}

func newWindow(app fyne.App, communication contract.Transport, positionResolver contract.PositionResolver, config *configuration.Configuration) (fyne.Window, chan contract.State) {
	lightBedStateChannel := make(chan contract.State)

	w := app.NewWindow("Light bed viewer")

	lightBedCanvas := light_bed.LightBedCanvas(
		lightBedStateChannel, positionResolver,
		config.LightBedProperties,
	)

	stateInfo := widget.NewLabel("Not connected")

	restartButton := widget.NewButton("Restart", func() {
		communication.Restart()
	})

	quitButton := widget.NewButton("Quit", func() {
		w.Close()
	})

	w.SetContent(
		fyne.NewContainerWithLayout(
			layout.NewBorderLayout(
				nil,
				quitButton,
				nil,
				nil,
			),
			fyne.NewContainerWithLayout(
				layout.NewBorderLayout(
					nil,
					restartButton,
					nil,
					nil,
				),
				fyne.NewContainerWithLayout(
					layout.NewBorderLayout(
						nil,
						stateInfo,
						nil,
						nil,
					),
					lightBedCanvas, stateInfo,
				), restartButton,
			), quitButton,
		),
	)

	w.Resize(fyne.Size{Width: 500, Height: 540})

	communication.AssignStatusBar(func(message string) {
		stateInfo.SetText(message)
	})

	communication.AssignChanel(lightBedStateChannel)

	w.SetOnClosed(func() {
		communication.Close()
	})

	return w, lightBedStateChannel
}
