package draw

import (
	"image"
	"image/color"
	"math"
)

func Dot(img image.RGBA, size int, positionX, positionY int, c color.Color) {
	for x := -size; x < size; x++ {
		for y := -size; y < size; y++ {
			if int(math.Sqrt(float64(x*x+y*y))) < size {
				img.Set(
					positionX+x,
					positionY+y,
					c,
				)
			}
		}
	}
}
