package main

import (
	"github.com/grandcat/zeroconf"
	"golang.org/x/net/context"
	"io"
	"log"
	"math/rand"
	"net"
	"strings"
	"time"
)

const (
	service  = "_lightbed._udp"
	domain   = "local."
	waitTime = 30
	tickTime = "2s"
)

func main() {
	ipOption := zeroconf.SelectIPTraffic(zeroconf.IPv4)

	resolver, err := zeroconf.NewResolver(
		ipOption,
	)
	if err != nil {
		log.Fatalln("Failed to initialize resolver:", err.Error())
	}

	zeroConfContext, cancel := context.WithCancel(context.Background())
	defer cancel()

	var sb strings.Builder
	sb.Write(
		[]byte{
			0, // device ID
			1, // bank
		},
	)

	step := float64(0x03FF) / 63

	for i := float64(0); i < 64; i++ {
		j := 0x03FF - uint16(step*i)

		sb.Write(
			[]byte{
				uint8(j >> 8), // MSB
				uint8(j),      // LSB
			},
		)
	}

	reader := strings.NewReader(sb.String())

	entries := make(chan *zeroconf.ServiceEntry)
	go func(results <-chan *zeroconf.ServiceEntry) {
		var udpContextCancel *context.CancelFunc

		for entry := range results {
			if udpContextCancel != nil {
				(*udpContextCancel)()
			}

			udpContext, _udpContextCancel := context.WithCancel(zeroConfContext)
			udpContextCancel = &_udpContextCancel

			udpAddr := &net.UDPAddr{
				IP:   entry.AddrIPv4[0],
				Port: entry.Port,
				Zone: "",
			}

			runClient := make(chan *error, 1)
			runClient <- nil

			for {
				select {
				case <-udpContext.Done():
					break

				case <-runClient:
					go func() {
						err := udpSender(udpContext, udpAddr, reader)

						if err != nil {
							log.Println("Failed to send UDP message:", err.Error())
						}

						time.Sleep(10 * time.Second)

						runClient <- &err
					}()
				}
			}
		}
	}(entries)

	err = resolver.Browse(zeroConfContext, service, domain, entries)
	if err != nil {
		log.Fatalln("Failed to browse:", err.Error())
	}

	duration, err := time.ParseDuration(tickTime)
	if err != nil {
		log.Fatalln("Failed to parse duration:", err.Error())
	}
	tick := time.NewTicker(duration)
	rand.Seed(time.Now().UTC().UnixNano())

	for {
		<-tick.C

		_, err := reader.Seek(0, io.SeekStart)

		if err != nil {
			log.Fatalln("Failed to seek reader", err.Error())
		}
	}

	// Wait some additional time to see debug messages on go routine shutdown.
	time.Sleep(1 * time.Second)
}

const (
	maxBufferSize = 1024
)

type MeasurableReader interface {
	Read(p []byte) (n int, err error)
	Len() int
}

// client wraps the whole functionality of a UDP client that sends
// a message and waits for a response coming back from the server
// that it initially targetted.
func udpSender(ctx context.Context, raddr *net.UDPAddr, reader MeasurableReader) error {
	// Although we're not in a connection-oriented transport,
	// the act of `dialing` is analogous to the act of performing
	// a `connect(2)` syscall for a socket of type SOCK_DGRAM:
	// - it forces the underlying socket to only read and write
	//   to and from a specific remote address.
	conn, err := net.DialUDP("udp4", nil, raddr)
	if err != nil {
		return err
	}

	// Closes the underlying file descriptor associated with the,
	// socket so that it no longer refers to any file.
	defer conn.Close()

	for {

		tick := time.NewTicker(time.Second)

		select {
		case <-tick.C:
			if reader.Len() == 0 {
				continue
			}

			// It is possible that this action blocks, although this
			// should only occur in very resource-intensive situations:
			// - when you've filled up the socket buffer and the OS
			//   can't dequeue the queue fast enough.
			n, err := io.Copy(conn, reader)
			if err != nil {
				return err
			}

			log.Printf("packet-written: bytes=%d\n", n)

		case <-ctx.Done():
			log.Println("cancelled")
			err = ctx.Err()
		}
	}

	return nil
}
