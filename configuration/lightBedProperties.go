package configuration

import "gitlab.com/cvut_magister/thesis/viewer/contract"

type LightBedProperties struct {
	BankCount contract.BankId
	Width     uint8
	Height    uint8

	PixelBlacklist [][]uint8
	RealSizeX      uint32
	RealSizeY      uint32

	IRLedPositionX       float64
	IRLedPositionY       float64
	IRLedPositionOffsetX float64
	IRLedPositionOffsetY float64

	TransistorPositionX       float64
	TransistorPositionY       float64
	TransistorPositionOffsetX float64
	TransistorPositionOffsetY float64

	pixelBlacklist map[contract.BankId]map[uint8]map[uint8]bool
}

func (lightBedProperties *LightBedProperties) Initialize() {
	lightBedProperties.pixelBlacklist = make(map[contract.BankId]map[uint8]map[uint8]bool)

	for _, i := range lightBedProperties.PixelBlacklist {
		bank := contract.BankId(i[0])
		x := i[1]
		y := i[2]

		_, ok := lightBedProperties.pixelBlacklist[bank]
		if !ok {
			lightBedProperties.pixelBlacklist[bank] = make(map[uint8]map[uint8]bool)
		}

		_, ok = lightBedProperties.pixelBlacklist[bank][x]
		if !ok {
			lightBedProperties.pixelBlacklist[bank][x] = make(map[uint8]bool)
		}

		lightBedProperties.pixelBlacklist[bank][x][y] = true
	}
}

func (lightBedProperties *LightBedProperties) IsBlacklisted(bank contract.BankId, x, y uint8) bool {
	_, ok := lightBedProperties.pixelBlacklist[bank]

	if !ok {
		return false
	}

	_, ok = lightBedProperties.pixelBlacklist[bank][x]

	if !ok {
		return false
	}

	blacklisted, ok := lightBedProperties.pixelBlacklist[bank][x][y]

	if !ok {
		return false
	}

	return blacklisted
}
