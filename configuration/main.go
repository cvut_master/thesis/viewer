package configuration

import (
	"fmt"
	"github.com/tkanos/gonfig"
	"gitlab.com/cvut_magister/thesis/viewer/contract"
	"os"
	"time"
)

type Configuration struct {
	Transport            contract.TransportType
	TransportArgs        transportArgs
	PositionResolver     contract.PositionResolverType
	PositionResolverArgs positionResolverArgs
	AllowedMemoryUsage   *uint64
	LightBedProperties   LightBedProperties
}

type transportArgs struct {
	SendEvery         *string
	SendEveryDuration *time.Duration
	StorePath         *string
	StoreTag          *string
	Port              *uint16
	Interfaces        []string
	AnnounceTimeout   *string
	AnnounceDuration  *time.Duration
	AnnounceAddress   []string
	AnnouncePort      int
	EnableZeroConfig  *bool
}

type positionResolverArgs struct {
	InterestCount           uint8
	BankCount               uint8
	OverdeterminedIteration uint8
	StorePath               string
	StoreTag                *string
}

func Build(environment string) (*Configuration, error) {
	config := &Configuration{}

	err := gonfig.GetConf("config.json", config)
	if err != nil {
		return nil, err
	}

	environmentFile := fmt.Sprintf("config.%s.json", environment)

	if _, err := os.Stat(environmentFile); !os.IsNotExist(err) {
		err = gonfig.GetConf(environmentFile, config)
		if err != nil {
			return nil, err
		}
	}

	if config.TransportArgs.SendEvery != nil {
		duration, err := time.ParseDuration(*config.TransportArgs.SendEvery)

		if err != nil {
			return nil, err
		}

		config.TransportArgs.SendEveryDuration = &duration
	}

	if config.TransportArgs.AnnounceTimeout != nil {
		duration, err := time.ParseDuration(*config.TransportArgs.AnnounceTimeout)

		if err != nil {
			return nil, err
		}

		config.TransportArgs.AnnounceDuration = &duration
	}

	config.LightBedProperties.Initialize()

	return config, err
}
