package light_bed

import (
	"fyne.io/fyne"
	"fyne.io/fyne/canvas"
	"fyne.io/fyne/theme"
	"fyne.io/fyne/widget"
	"gitlab.com/cvut_magister/thesis/viewer/configuration"
	"gitlab.com/cvut_magister/thesis/viewer/contract"
	"gitlab.com/cvut_magister/thesis/viewer/draw"
	"image"
	"image/color"
)

type lightBed struct {
	transmitter map[contract.BankId][][]uint16
	emitter     [][]uint16

	// position is stored in mm
	positionX *float64
	positionY *float64

	stateChannel     <-chan contract.State
	positionResolver contract.PositionResolver

	width  uint8
	height uint8

	upScaleWidth  uint8
	upScaleHeight uint8

	lightBedProperties configuration.LightBedProperties
}

func newBed(
	lightBedStateChannel <-chan contract.State, positionResolver contract.PositionResolver,
	lightBedProperties configuration.LightBedProperties,
) *lightBed {
	b := &lightBed{
		width: lightBedProperties.Width, height: lightBedProperties.Height,
		upScaleWidth: lightBedProperties.Width, upScaleHeight: lightBedProperties.Height, // expect transmitters and emitters have same dimension
		lightBedProperties: lightBedProperties,
	}
	banks := contract.BankId(b.upScaleWidth*b.upScaleHeight + 1)

	b.transmitter = make(map[contract.BankId][][]uint16, lightBedProperties.BankCount)

	b.emitter = make([][]uint16, b.height)
	b.positionResolver = positionResolver
	b.stateChannel = lightBedStateChannel

	var i uint16
	k := 0xffff / uint16(b.height*b.width)

	for bank := contract.BankId(0); bank < banks; bank++ {
		i = 0
		b.transmitter[bank] = make([][]uint16, banks)

		for y := uint8(0); y < b.height; y++ {
			b.transmitter[bank][y] = make([]uint16, b.width)
			b.emitter[y] = make([]uint16, b.width)

			for x := uint8(0); x < b.width; x++ {
				b.transmitter[bank][y][x] = i

				if bank == 0 {
					i = i + k
				} else {
					i = i + k/10
				}
			}
		}
	}

	return b
}

func (lightBed *lightBed) update(state contract.State) bool {
	if uint8(len(state.TransmitterState)) != lightBed.height {
		return false
	}

	for y := uint8(0); y < lightBed.height; y++ {
		if uint8(len(state.TransmitterState[y])) != lightBed.width {
			return false
		}
	}

	for y := uint8(0); y < lightBed.height; y++ {
		for x := uint8(0); x < lightBed.width; x++ {
			if state.TransmitterState[y][x] != nil {
				lightBed.transmitter[state.Bank][y][x] = *state.TransmitterState[y][x]
			}
		}
	}

	lightBed.positionX, lightBed.positionY = lightBed.positionResolver.Update(lightBed.transmitter, state.Bank)

	return true
}

type lightBedState struct {
	fyne.CanvasObject

	lightBed *lightBed

	size     fyne.Size
	position fyne.Position
	hidden   bool
}

func newLightBedState(lightBed *lightBed) *lightBedState {
	lightBedState := &lightBedState{lightBed: lightBed}

	go lightBedState.updater()

	return lightBedState
}

func (lightBedState *lightBedState) updater() {
	for {
		if lightBedState.lightBed.update(
			<-lightBedState.lightBed.stateChannel,
		) {
			canvas.Refresh(lightBedState)
		}
	}
}

func (lightBedState *lightBedState) cellForCoordinates(x, y, w, h int) (uint8, uint8, contract.BankId) {
	lightBed := lightBedState.lightBed

	xPos := uint8(float64(lightBed.width) * (float64(x) / float64(w)))
	yPos := uint8(float64(lightBed.height) * (float64(y) / float64(h)))

	xPosUpscale := uint8(float64(lightBed.width*(lightBed.upScaleWidth+1)) * (float64(x) / float64(w)))
	yPosUpscale := uint8(float64(lightBed.height*lightBed.upScaleHeight) * (float64(y) / float64(h)))

	xPosUpscaleMod := xPosUpscale - xPos*(lightBed.upScaleWidth+1)

	var bank contract.BankId
	if xPosUpscaleMod == 0 {
		bank = 0
	} else {
		bank = contract.BankId(xPosUpscaleMod + lightBed.upScaleWidth*(yPosUpscale-yPos*lightBed.upScaleHeight))
	}

	return xPos, yPos, bank
}

func (lightBedState *lightBedState) CreateRenderer() fyne.WidgetRenderer {
	renderer := &lightBedRenderer{lightBedState: lightBedState}

	render := canvas.NewRaster(renderer.draw)
	renderer.render = render
	renderer.objects = []fyne.CanvasObject{render}
	renderer.ApplyTheme()

	return renderer
}

func (lightBedState *lightBedState) Size() fyne.Size {
	return lightBedState.size
}

func (lightBedState *lightBedState) Resize(size fyne.Size) {
	lightBedState.size = size
	widget.Renderer(lightBedState).Layout(size)
}

func (lightBedState *lightBedState) Position() fyne.Position {
	return lightBedState.position
}

func (lightBedState *lightBedState) Move(pos fyne.Position) {
	lightBedState.position = pos
	widget.Renderer(lightBedState).Layout(lightBedState.size)
}

func (lightBedState *lightBedState) MinSize() fyne.Size {
	return widget.Renderer(lightBedState).MinSize()
}

func (lightBedState *lightBedState) Visible() bool {
	return lightBedState.hidden
}

func (lightBedState *lightBedState) Show() {
	lightBedState.hidden = false
}

func (lightBedState *lightBedState) Hide() {
	lightBedState.hidden = true
}

type lightBedRenderer struct {
	lightBedState *lightBedState

	objects []fyne.CanvasObject
	render  *canvas.Raster

	backgroundColor color.Color

	imgCache *image.RGBA
}

func (lightBedRenderer *lightBedRenderer) Layout(size fyne.Size) {
	lightBedRenderer.render.Resize(size)
}

func (lightBedRenderer *lightBedRenderer) MinSize() fyne.Size {
	return fyne.NewSize(
		int(lightBedRenderer.lightBedState.lightBed.width)*10,
		int(lightBedRenderer.lightBedState.lightBed.height)*10,
	)
}

func (lightBedRenderer *lightBedRenderer) Refresh() {

}

func (lightBedRenderer *lightBedRenderer) ApplyTheme() {
	lightBedRenderer.backgroundColor = theme.BackgroundColor()
}

func (lightBedRenderer *lightBedRenderer) BackgroundColor() color.Color {
	return lightBedRenderer.backgroundColor
}

func (lightBedRenderer *lightBedRenderer) Objects() []fyne.CanvasObject {
	return lightBedRenderer.objects
}

func (lightBedRenderer *lightBedRenderer) Destroy() {

}

func (lightBedRenderer *lightBedRenderer) draw(w int, h int) image.Image {
	img := lightBedRenderer.imgCache
	if img == nil || img.Bounds().Size().X != w || img.Bounds().Size().Y != h {
		img = image.NewRGBA(
			image.Rect(0, 0, w, h),
		)
		lightBedRenderer.imgCache = img
	}

	lightBedProperties := lightBedRenderer.lightBedState.lightBed.lightBedProperties

	for y := 0; y < h; y++ {
		for x := 0; x < w; x++ {
			xpos, ypos, bank := lightBedRenderer.lightBedState.cellForCoordinates(x, y, w, h)

			var c color.RGBA64

			if bank == 0 {
				c = color.RGBA64{
					R: lightBedRenderer.lightBedState.lightBed.transmitter[bank][ypos][xpos],
					G: 0xffff - lightBedRenderer.lightBedState.lightBed.transmitter[bank][ypos][xpos],
					B: 0x0,
					A: 0xffff,
				}
			} else {
				r := uint32(lightBedRenderer.lightBedState.lightBed.transmitter[bank][ypos][xpos]) * 10
				var b uint16 = 0

				if r > 0xffff {
					b = uint16(0x0000ffff - r)
					r = 0xffff
				}

				c = color.RGBA64{
					R: uint16(r),
					G: 0xffff - uint16(r),
					B: b,
					A: 0xffff,
				}
			}

			if lightBedProperties.IsBlacklisted(bank, xpos, ypos) {
				c = color.RGBA64{
					R: 0x0,
					G: 0x0,
					B: 0xffff,
					A: 0xffff,
				}
			}
			/*else if bank > 0 && lightBedRenderer.lightBedState.lightBed.transmitter[bank][ypos][xpos] > 1024 {
				fmt.Printf("[%d, %d, %d],\n", bank, xpos, ypos)

				_, ok := lightBedRenderer.lightBedState.lightBed.pixelBlacklist[bank]
				if !ok {
					lightBedRenderer.lightBedState.lightBed.pixelBlacklist[bank] = make(map[uint8]map[uint8]bool)
				}

				_, ok = lightBedRenderer.lightBedState.lightBed.pixelBlacklist[bank][xpos]
				if !ok {
					lightBedRenderer.lightBedState.lightBed.pixelBlacklist[bank][xpos] = make(map[uint8]bool)
				}

				lightBedRenderer.lightBedState.lightBed.pixelBlacklist[bank][xpos][ypos] = true
			}
			*/

			img.Set(x, y, c)
		}
	}

	activePositionResolver, isActivePositionResolverType := lightBedRenderer.lightBedState.lightBed.positionResolver.(contract.ActivePositionResolver)
	if isActivePositionResolverType && activePositionResolver != nil {
		activePositionResolver.Draw(*img, w, h)
	}

	if lightBedRenderer.lightBedState.lightBed.positionX != nil && lightBedRenderer.lightBedState.lightBed.positionY != nil {
		positionOnImageX := int(float64(w) * (*lightBedRenderer.lightBedState.lightBed.positionX) / float64(lightBedProperties.RealSizeX))
		positionOnImageY := int(float64(h) * (*lightBedRenderer.lightBedState.lightBed.positionY) / float64(lightBedProperties.RealSizeY))

		dotSize := int(w / 50)

		draw.Dot(*img, dotSize, positionOnImageX, positionOnImageY, color.Black)
	}

	return img
}

func LightBedCanvas(
	lightBedStateChannel <-chan contract.State, positionResolver contract.PositionResolver,
	lightBedProperties configuration.LightBedProperties,
) fyne.CanvasObject {
	bed := newBed(
		lightBedStateChannel, positionResolver,
		lightBedProperties,
	)

	lightBed := newLightBedState(bed)

	return lightBed
}
