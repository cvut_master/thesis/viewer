#!/bin/bash

set -e

DIR=`dirname "$0"`
FILE=bundled.go
BIN=`go env GOPATH`/bin

cd $DIR
if [[ -f $FILE ]]; then
    rm $FILE
fi

$BIN/fyne bundle -package icon -name lifeBitmap life.png >> $FILE
# $BIN/fyne bundle -package icon -append -name lifeBitmap life.png >> $FILE

