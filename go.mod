module gitlab.com/cvut_magister/thesis/viewer

go 1.12

require (
	fyne.io/fyne v1.0.0
	github.com/cenkalti/backoff v2.1.1+incompatible // indirect
	github.com/ghodss/yaml v1.0.0 // indirect
	github.com/go-gl/gl v0.0.0-20190320180904-bf2b1f2f34d7 // indirect
	github.com/go-gl/glfw v0.0.0-20190409004039-e6da0acd62b1 // indirect
	github.com/goki/freetype v0.0.0-20181231101311-fa8a33aabaff // indirect
	github.com/grandcat/zeroconf v0.0.0-20190118114326-c2d1b4121200
	github.com/miekg/dns v1.1.8 // indirect
	github.com/srwiley/oksvg v0.0.0-20190414003808-c520f0a6c5cc // indirect
	github.com/srwiley/rasterx v0.0.0-20181219215540-696f7edb7a7e // indirect
	github.com/stretchr/testify v1.3.0 // indirect
	github.com/tkanos/gonfig v0.0.0-20181112185242-896f3d81fadf
	golang.org/x/image v0.0.0-20190321063152-3fc05d484e9f // indirect
	golang.org/x/net v0.0.0-20190415214537-1da14a5a36f2
	golang.org/x/sync v0.0.0-20190412183630-56d357773e84 // indirect
	gonum.org/v1/gonum v0.0.0-20190517105223-d76380b39392
	gopkg.in/yaml.v2 v2.2.2 // indirect
)
