package contract

type BankId uint8
type State struct {
	Device           uint8
	Bank             BankId
	TransmitterState [][]*uint16
}

type Transport interface {
	AssignChanel(channel chan<- State)
	AssignStatusBar(func(message string))
	Restart()
	Close()
}

type TransportType string

const (
	FakeServer      TransportType = "FakeServer"
	RestoringServer TransportType = "RestoringServer"
	UdpServer       TransportType = "UdpServer"
)
