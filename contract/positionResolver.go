package contract

import "image"

type PositionResolver interface {
	Update(transmitter map[BankId][][]uint16, updatedBank BankId) (*float64, *float64)
}

type ActivePositionResolver interface {
	Draw(img image.RGBA, w int, h int)

	PositionResolver
}

type PositionResolverType string

const (
	NaivePositionResolver   PositionResolverType = "NaivePositionResolver"
	StoringPositionResolver PositionResolverType = "StoringPositionResolver"
)
