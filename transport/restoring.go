package transport

import (
	"encoding/gob"
	"fmt"
	"gitlab.com/cvut_magister/thesis/viewer/contract"
	"gitlab.com/cvut_magister/thesis/viewer/position_resolver"
	"io/ioutil"
	"log"
	"os"
	"sort"
	"time"
)

type restoringServer struct {
	channel          *chan<- contract.State
	statusBarUpdater func(message string)

	duration time.Duration
	path     string
	storeTag *string
}

// demonstration of receiving message with partial data
func NewRestoringServer(
	duration *time.Duration, path string,
	storeTag *string,
) contract.Transport {
	server := &restoringServer{
		statusBarUpdater: func(message string) {
			log.Print(message)
		},
		path:     path,
		storeTag: storeTag,
	}

	if duration == nil {
		server.duration = time.Second
	} else {
		server.duration = *duration
	}

	go server.run()

	return server
}

func (restoringServer *restoringServer) AssignChanel(channel chan<- contract.State) {
	restoringServer.channel = &channel
}

func (restoringServer *restoringServer) AssignStatusBar(statusBarUpdater func(message string)) {
	restoringServer.statusBarUpdater = statusBarUpdater
}

func (restoringServer *restoringServer) Restart() {

}

func (restoringServer *restoringServer) Close() {

}

func (restoringServer *restoringServer) run() {
	// tick := time.NewTicker(restoringServer.duration)

	folder := restoringServer.path

	if restoringServer.storeTag != nil {
		folder = fmt.Sprintf("%s/%s", folder, *restoringServer.storeTag)
	}

	var lastTime *time.Time

	files, err := ioutil.ReadDir(folder)
	if err != nil {
		log.Fatal(err.Error())
	}

	for _, f := range files {
		file, err := os.Open(fmt.Sprintf("%s/%s", folder, f.Name()))
		if err != nil {
			log.Fatal(err.Error())
		}

		decoder := gob.NewDecoder(file)
		nodes := make(map[int]position_resolver.HistoryNode)
		err = decoder.Decode(&nodes)
		if err != nil {
			log.Print(err.Error())
			file.Close()

			continue
		}
		file.Close()

		restoringServer.statusBarUpdater(fmt.Sprintf("Processing file %s", f.Name()))

		nodeKeys := make([]int, 0)
		for i, _ := range nodes {
			nodeKeys = append(nodeKeys, i)
		}
		sort.Ints(nodeKeys)

		for _, i := range nodeKeys {
			node := nodes[i]
			if lastTime == nil {
				lastTime = &node.Time
			}
			<-time.After(
				node.Time.Sub(*lastTime),
			)

			lastTime = &node.Time

			if restoringServer.channel != nil {
				*restoringServer.channel <- contract.State{Bank: node.UpdatedBank, TransmitterState: node.Transmitter}
			}

			if node.UpdatedBank == 64 {
				<-time.After(1 * time.Millisecond)
			}
		}
	}

	restoringServer.statusBarUpdater("No more stored files")
}
