package transport

import (
	"fmt"
	"gitlab.com/cvut_magister/thesis/viewer/contract"
	"math"
	"math/rand"
	"time"
)

type fakeServer struct {
	channel          *chan<- contract.State
	statusBarUpdater func(message string)

	duration time.Duration
}

// demonstration of receiving message with partial data
func NewFakeServer(duration *time.Duration) contract.Transport {
	server := &fakeServer{
		statusBarUpdater: func(string) {},
	}

	if duration == nil {
		server.duration = time.Second
	} else {
		server.duration = *duration
	}

	go server.run()

	return server
}

func (fakeServer *fakeServer) AssignChanel(channel chan<- contract.State) {
	fakeServer.channel = &channel
}

func (fakeServer *fakeServer) AssignStatusBar(statusBarUpdater func(message string)) {
	fakeServer.statusBarUpdater = statusBarUpdater
}

func (fakeServer *fakeServer) Restart() {

}

func (fakeServer *fakeServer) Close() {

}

func (fakeServer *fakeServer) run() {
	tick := time.NewTicker(fakeServer.duration)

	var i uint8 = 0

	rand.Seed(time.Now().UTC().UnixNano())
	update := make([][]*uint16, 8)
	for k := uint8(0); k < 8; k++ {
		update[k] = make([]*uint16, 8)
	}

	for {
		<-tick.C

		if fakeServer.channel == nil {
			continue
		}

		if i > 63 {
			i = 0
		}

		x := uint8(math.Floor(float64(i) / 8))
		y := i - x*8

		r := uint16(rand.Uint32() % 0xffff)
		bank := contract.BankId(rand.Uint32() % 65)

		update[y][x] = &r
		*fakeServer.channel <- contract.State{Bank: bank, TransmitterState: update}

		fakeServer.statusBarUpdater(fmt.Sprintf("Updated bank %d", i))

		i++
	}

}
