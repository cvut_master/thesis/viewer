package transport

import (
	"context"
	"fmt"
	"github.com/grandcat/zeroconf"
	"gitlab.com/cvut_magister/thesis/viewer/contract"
	"io"
	"log"
	"math"
	"net"
	"strings"
	"sync"
	"time"
)

type udpServer struct {
	channel          *chan<- contract.State
	statusBarUpdater func(message string)

	port       uint16
	interfaces []string

	usedAddress     map[string]*time.Time
	usedAddressLock sync.RWMutex

	announceAddress []string
	announcePort    int

	close bool
}

func NewUdpServer(
	port uint16, interfaces []string,
	announceDuration time.Duration, announceAddress []string, announcePort int,
	enableZeroConfig *bool,
) contract.Transport {
	if enableZeroConfig == nil {
		_enableZeroConfig := false
		enableZeroConfig = &_enableZeroConfig
	}

	server := &udpServer{
		port:             port,
		interfaces:       interfaces,
		usedAddress:      make(map[string]*time.Time),
		usedAddressLock:  sync.RWMutex{},
		statusBarUpdater: func(string) {},

		announceAddress: announceAddress,
		announcePort:    announcePort,

		close: false,
	}

	go server.announce(announceDuration)

	go server.run(*enableZeroConfig)

	return server
}

func (server *udpServer) AssignChanel(channel chan<- contract.State) {
	server.channel = &channel
}

func (server *udpServer) AssignStatusBar(statusBarUpdater func(message string)) {
	server.statusBarUpdater = statusBarUpdater
}

func (server *udpServer) Restart() {
	server.usedAddressLock.Lock()
	defer server.usedAddressLock.Unlock()

	ci := make([]chan bool, len(server.usedAddress))
	var i uint = 0
	for address, _ := range server.usedAddress {
		ci[i] = make(chan bool)

		go announcePort(server, address, server.announcePort, 0, ci[i])
		go func() {
			<-time.After(10 * time.Millisecond)

			server.usedAddress[address] = nil
		}()

		i++
	}

	for _, c := range ci {
		<-c
	}
}

func (server *udpServer) Close() {
	server.close = true

	server.usedAddressLock.Lock()
	defer server.usedAddressLock.Unlock()

	ci := make([]chan bool, len(server.usedAddress))
	var i uint = 0
	for address, _ := range server.usedAddress {
		ci[i] = make(chan bool)

		go announcePort(server, address, server.announcePort, 0, ci[i])
		go func() {
			<-time.After(10 * time.Millisecond)

			server.usedAddress[address] = nil
		}()

		i++
	}

	for _, c := range ci {
		<-c
	}
}

const (
	maxBufferSize           = 1024
	expectedSize            = 130
	controlByte             = 2
	rowsCount               = 8
	colsCount               = 8
	announceReplyBufferSize = 22
	announceReplyTimeout    = time.Second
)

func (server *udpServer) announce(announceDuration time.Duration) {
	tick := time.NewTicker(announceDuration)

	timeout := time.Second * 30

	for {
		<-tick.C

		if server.close {
			break
		}

		server.usedAddressLock.RLock()
		oneAlive := false
		for _, t := range server.usedAddress {
			if t != nil && t.Add(timeout).After(time.Now()) {
				oneAlive = true
				break
			}
		}
		server.usedAddressLock.RUnlock()

		if oneAlive {
			continue
		}

		server.statusBarUpdater(
			fmt.Sprintf("Announcing viewer to %s", strings.Join(server.announceAddress, ", ")),
		)

		ci := make([]chan bool, len(server.announceAddress))

		for i, address := range server.announceAddress {
			ci[i] = make(chan bool)

			go announcePort(server, address, server.announcePort, server.port, ci[i])
		}

		for _, c := range ci {
			<-c
		}
	}
}

func announcePort(server *udpServer, address string, announcePort int, targetPort uint16, c chan<- bool) {
	ip := net.ParseIP(address)

	udpAddr := &net.UDPAddr{
		IP:   ip,
		Port: announcePort,
		Zone: "",
	}

	udpContext, udpContextCancel := context.WithTimeout(context.Background(), time.Second*10)
	defer (udpContextCancel)()

	err := server.udpSender(udpContext, udpAddr, strings.NewReader(fmt.Sprintf("0.0.0.0:%d\n", targetPort))) // maybe send preferred IP address?
	if err != nil {
		log.Println("Announce failed: ", err.Error())

		c <- false
		return
	}

	c <- true
}

// client wraps the whole functionality of a UDP client that sends
// a message and waits for a response coming back from the server
// that it initially targetted.
func (server *udpServer) udpSender(ctx context.Context, raddr *net.UDPAddr, reader io.Reader) error {
	// Although we're not in a connection-oriented transport,
	// the act of `dialing` is analogous to the act of performing
	// a `connect(2)` syscall for a socket of type SOCK_DGRAM:
	// - it forces the underlying socket to only read and write
	//   to and from a specific remote address.
	conn, err := net.DialUDP("udp", nil, raddr)
	if err != nil {
		return err
	}

	// Closes the underlying file descriptor associated with the,
	// socket so that it no longer refers to any file.
	defer conn.Close()

	for {
		select {
		default:
			// It is possible that this action blocks, although this
			// should only occur in very resource-intensive situations:
			// - when you've filled up the socket buffer and the OS
			//   can't dequeue the queue fast enough.
			n, err := io.Copy(conn, reader)
			if err != nil {
				return err
			}

			log.Printf("packet-written: bytes=%d\n", n)

			buffer := make([]byte, announceReplyBufferSize)

			// wait for reply so network do not produce ICMP error, it is also nice to see that everything was parsed correctly
			deadline := time.Now().Add(announceReplyTimeout)
			err = conn.SetReadDeadline(deadline)
			if err != nil {
				return err
			}

			nRead, addr, err := conn.ReadFrom(buffer)
			if err != nil {
				return err
			}

			log.Printf("packet-received: bytes=%d from=%s\n%s\n", nRead, addr.String(), buffer[:nRead])

			return nil

		case <-ctx.Done():
			log.Println("cancelled")
			return ctx.Err()
		}
	}
}

func (server *udpServer) run(enableZeroConfig bool) {
	interfaces := server.filterMulticastInterfaces()

	if enableZeroConfig {
		zeroConfServer, err := zeroconf.Register("LightBed Viewer - UDP", "_lightbed._udp", "local.", int(server.port), nil, interfaces)
		if err != nil {
			panic(err)
		}
		defer zeroConfServer.Shutdown()

		zeroConfServer.TTL(60)
	}

	conn, err := net.ListenUDP("udp4", &net.UDPAddr{
		Port: int(server.port),
		IP:   net.ParseIP("0.0.0.0"),
	})
	if err != nil {
		log.Fatalln("Unable to start UDP server", err.Error())
	}
	defer conn.Close()

	log.Printf("UDP server listening %s\n", conn.LocalAddr().String())

	message := make([]byte, maxBufferSize)

	var rlen int
	var remote *net.UDPAddr

	var rateCounter uint = 0
	var rateRepetitiveZeroCounter uint = 0

	go func() {
		tick := time.NewTicker(time.Second)

		for {
			<-tick.C

			if server.close {
				break
			}

			if remote != nil {
				rateCounterShadow := float64(rateCounter)
				rateCounter = 0

				if rateCounterShadow == 0 {
					rateRepetitiveZeroCounter++
				} else {
					rateRepetitiveZeroCounter = 0
				}

				if rateRepetitiveZeroCounter > 5 {
					server.statusBarUpdater("Not connected")

					continue
				}

				server.statusBarUpdater(fmt.Sprintf("LightBed %s sending %3.3f message/sec", remote.IP.String(), rateCounterShadow/60))
			}
		}
	}()

	var i uint64 = 0
	for {
		rlen, remote, err = conn.ReadFromUDP(message)
		if err != nil {
			log.Fatalln("Error UDP socket read: ", err.Error())
		}

		if server.close {
			break
		}

		if remote.IP.Equal(net.IPv4zero) {
			log.Println("Message with zero IP address, skipping")

			//continue
		}

		rateCounter++

		now := time.Now()
		server.usedAddressLock.Lock()
		server.usedAddress[remote.IP.String()] = &now
		server.usedAddressLock.Unlock()

		processMessage(server, message, rlen)

		if i >= math.MaxUint64 {
			i = 0
		}

		i++
	}
}

func processMessage(server *udpServer, message []byte, rlen int) {
	if rlen != expectedSize {
		log.Printf("Unexpected message from light bed with length %d, expected %d\n", rlen, expectedSize)

		return
	}

	deviceId := message[0]
	bankId := contract.BankId(message[1])

	// TODO log levels?
	// log.Printf("%d: received message len(%d) from device %d-%d %s\n", i, rlen, deviceId, bankId, remote)

	measurements := message[2:expectedSize]

	data := make([]*uint16, (expectedSize-controlByte)/2)

	for m := 0; m < len(data); m++ {
		singleMeasurement := uint16(measurements[m*2])<<8 + uint16(measurements[m*2+1])

		if singleMeasurement < 0x0400 { // 10bits
			singleMeasurement = singleMeasurement << 6

			data[m] = &singleMeasurement
		} else {
			data[m] = nil
		}
	}

	receivedState := make([][]*uint16, rowsCount)
	for m := 0; m < rowsCount; m++ {
		receivedState[m] = data[m*colsCount : (m+1)*colsCount]
	}

	if server.channel != nil {
		*server.channel <- contract.State{Device: deviceId, Bank: bankId, TransmitterState: receivedState}

		if bankId == 64 {
			<-time.After(100 * time.Microsecond) // force context switch
		}
	}
}

func (server *udpServer) filterMulticastInterfaces() []net.Interface {
	if len(server.interfaces) == 0 {
		return nil
	}

	var interfaces []net.Interface
	ifaces, err := net.Interfaces()
	if err != nil {
		return nil
	}
	for _, ifi := range ifaces {
		if (ifi.Flags & net.FlagUp) == 0 {
			continue
		}
		if (ifi.Flags & net.FlagMulticast) > 0 {
			for _, interfaceName := range server.interfaces {
				if interfaceName == ifi.Name {
					interfaces = append(interfaces, ifi)
				}
			}
		}
	}

	return interfaces
}
