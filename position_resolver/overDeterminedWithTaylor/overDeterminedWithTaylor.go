package overDeterminedWithTaylor

import "gonum.org/v1/gonum/mat"

func getBi(
	e uint16, e0,
	a1, a2, // Taylor initial condition
	x1, y1, // emitter position
	x2, y2 float64, // transmitter position
) float64 {
	var bi float64 = 0

	if e > 0 {
		bi = e0 / float64(e)
	}

	bi -= 2*a1*a1 + 2*a2*a2 + x1*x1 + x2*x2 - 2*a1*(x1+x2) - 2*a2*(y1+y2) + y1*y1 + y2*y2 // T0
	bi += a1*(4*a1-2*x1-2*x2) + a2*(4*a2-2*y1-2*y2)                                       // T1

	return bi
}

func GetB(
	e []uint16, e0 []float64,
	a1, a2, // Taylor initial condition
	x1, y1, // emitter position
	x2, y2 []float64, // transmitter position
) *mat.Dense {
	bArr := make([]float64, len(e))
	for i := 0; i < len(e); i++ {
		bArr[i] = getBi(
			e[i], e0[i],
			a1[i], a2[i],
			x1[i], y1[i],
			x2[i], y2[i],
		)
	}

	b := mat.NewDense(len(e), 1, bArr)

	return b
}

func getAi(
	e uint16, e0 float64,
	a1, a2, // Taylor initial condition
	x1, y1, // emitter position
	x2, y2 float64, // transmitter position
) (float64, float64) {
	var aX float64 = 0
	var aY float64 = 0

	aX += 4*a1 - 2*x1 - 2*x2
	aY += 4*a2 - 2*y1 - 2*y2

	return aX, aY
}

func GetA(
	e []uint16, e0 []float64,
	a1, a2, // Taylor initial condition
	x1, y1, // emitter position
	x2, y2 []float64, // transmitter position
) *mat.Dense {
	aArr := make([]float64, 2*len(e))
	for i := 0; i < len(e); i++ {
		aArr[i*2], aArr[i*2+1] = getAi(
			e[i], e0[i],
			a1[i], a2[i],
			x1[i], y1[i],
			x2[i], y2[i],
		)
	}

	A := mat.NewDense(len(e), 2, aArr)

	return A
}
