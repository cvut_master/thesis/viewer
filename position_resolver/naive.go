package position_resolver

import (
	"gitlab.com/cvut_magister/thesis/viewer/configuration"
	"gitlab.com/cvut_magister/thesis/viewer/contract"
	"gitlab.com/cvut_magister/thesis/viewer/draw"
	"gitlab.com/cvut_magister/thesis/viewer/position_resolver/overDeterminedWithTaylor"
	"gonum.org/v1/gonum/mat"
	"image"
	"image/color"
	"log"
	"math"
	"sort"
	"sync"
)

type naivePositionResolver struct {
	biggestExtreme          extremeMap
	maxKnownEnergy          map[contract.BankId]map[uint8]map[uint8]uint16
	maxKnownUnitEnergy      map[contract.BankId]map[uint8]map[uint8]float64
	interestCount           uint8
	bankCount               uint8
	overdeterminedIteration uint8

	biggestExtremeLock sync.RWMutex
	maxKnownValueLock  sync.RWMutex

	x *float64
	y *float64

	lightBedProperties configuration.LightBedProperties
}

type extreme struct {
	x     uint8
	y     uint8
	value uint16

	sourceX float64
	sourceY float64

	transmitterX float64
	transmitterY float64
}

type extremeList []extreme
type extremeMap map[contract.BankId]extremeList

func (extremeList extremeList) Len() int { return len(extremeList) }
func (extremeList extremeList) Swap(i, j int) {
	extremeList[i], extremeList[j] = extremeList[j], extremeList[i]
}
func (extremeList extremeList) Less(i, j int) bool { return extremeList[i].value < extremeList[j].value }

type significant struct {
	bankId contract.BankId
	value  uint16
}
type significantList []significant

func (list significantList) Len() int { return len(list) }
func (list significantList) Swap(i, j int) {
	list[i], list[j] = list[j], list[i]
}
func (list significantList) Less(i, j int) bool { return list[i].value < list[j].value }

func (positionResolver *naivePositionResolver) Update(transmitter map[contract.BankId][][]uint16, updatedBank contract.BankId) (*float64, *float64) {
	if updatedBank == 0 {
		return positionResolver.x, positionResolver.y
	}

	biggestExtreme := make(extremeList, positionResolver.interestCount)

	positionResolver.maxKnownValueLock.Lock()

	for y_, yVal := range transmitter[updatedBank] {
		y := uint8(y_)
		for x_, val := range yVal {
			x := uint8(x_)
			if positionResolver.lightBedProperties.IsBlacklisted(updatedBank, x, y) {
				continue
			}

			if val > positionResolver.maxKnownEnergy[updatedBank][x][y] {
				positionResolver.maxKnownEnergy[updatedBank][x][y] = val

				sourceX, sourceY := positionResolver.getSourcePosition(updatedBank)
				transmitterX, transmitterY := positionResolver.getTransmitterPosition(x, y)

				sourceTransmitterSquaredDistance := math.Pow(float64(sourceX-transmitterX), 2) + math.Pow(float64(sourceY-transmitterY), 2)
				positionResolver.maxKnownUnitEnergy[updatedBank][x][y] = float64(val) / sourceTransmitterSquaredDistance
			}

			sort.Sort(biggestExtreme)

			if biggestExtreme[0].value < val {
				biggestExtreme[0] = extreme{
					x:     x,
					y:     y,
					value: val,
				}
			}
		}
	}
	positionResolver.maxKnownValueLock.Unlock()

	sort.Sort(biggestExtreme)

	for k, item := range biggestExtreme {
		biggestExtreme[k].sourceX, biggestExtreme[k].sourceY = positionResolver.getSourcePosition(updatedBank)
		biggestExtreme[k].transmitterX, biggestExtreme[k].transmitterY = positionResolver.getTransmitterPosition(item.x, item.y)
	}

	positionResolver.biggestExtremeLock.Lock()
	positionResolver.biggestExtreme[updatedBank] = biggestExtreme
	positionResolver.biggestExtremeLock.Unlock()

	positionResolver.ResolvePosition()

	return positionResolver.x, positionResolver.y
}

func (positionResolver *naivePositionResolver) ResolvePosition() {
	positionResolver.biggestExtremeLock.RLock()
	biggestExtreme := positionResolver.biggestExtreme
	positionResolver.biggestExtremeLock.RUnlock()

	positionResolver.maxKnownValueLock.RLock()
	maxKnownUnitEnergy := positionResolver.maxKnownUnitEnergy
	positionResolver.maxKnownValueLock.RUnlock()

	if biggestExtreme == nil || maxKnownUnitEnergy == nil {
		return
	}

	significant := make(significantList, positionResolver.bankCount)

	var biggestValue uint16 = 0
	var biggestValueExtreme extreme
	for bankId, bankVal := range biggestExtreme {
		sort.Sort(significant)

		for _, iVal := range bankVal {
			if significant[0].value < iVal.value {
				significant[0].bankId = bankId
				significant[0].value = iVal.value
			}

			if biggestValue < iVal.value {
				biggestValue = iVal.value
				biggestValueExtreme = iVal
			}
		}
	}

	if biggestValueExtreme.value == 0 {

	}
	//l := math.Floor(math.Log10(float64(biggestValue)))
	allowedValueThreshold := biggestValue - biggestValue/5
	allowedValueThreshold = 0

	sourceTransmitterSquaredDistance := math.Pow(float64(biggestValueExtreme.sourceX-biggestValueExtreme.transmitterX), 2) + math.Pow(float64(biggestValueExtreme.sourceY-biggestValueExtreme.transmitterY), 2)
	knownUnitEnergy := float64(biggestValue) / sourceTransmitterSquaredDistance

	if knownUnitEnergy > 0 {

	}
	// log.Printf("%d", importantBank[0])

	occurrenceCount := 0
	for bankId, bankVal := range biggestExtreme {
		for _, significantItem := range significant {
			if significantItem.bankId == bankId {
				for _, iVal := range bankVal {
					//for range bankVal {
					if allowedValueThreshold <= iVal.value {
						occurrenceCount++
					}
				}
			}
		}
	}

	if occurrenceCount == 0 {
		return
	}

	e := make([]uint16, occurrenceCount)
	e0 := make([]float64, occurrenceCount)
	a1 := make([]float64, occurrenceCount)
	a2 := make([]float64, occurrenceCount)
	x1 := make([]float64, occurrenceCount)
	y1 := make([]float64, occurrenceCount)
	x2 := make([]float64, occurrenceCount)
	y2 := make([]float64, occurrenceCount)

	var posX, posY float64
	for i := uint8(0); i < positionResolver.overdeterminedIteration; i++ {
		occurrenceIndex := 0
		for _, significantItem := range significant {
			bankVal := biggestExtreme[significantItem.bankId]

			if bankVal == nil {
				continue
			}

			for _, iVal := range bankVal {
				if allowedValueThreshold > iVal.value {
					continue
				}

				e[occurrenceIndex] = iVal.value
				//e0[occurrenceIndex] = knownUnitEnergy
				e0[occurrenceIndex] = maxKnownUnitEnergy[significantItem.bankId][iVal.x][iVal.y]
				if i == 0 {
					if positionResolver.x == nil && positionResolver.y == nil {
						a1[occurrenceIndex] = iVal.transmitterX // initial condition in transmitter
						a2[occurrenceIndex] = iVal.transmitterY
					} else {
						a1[occurrenceIndex] = *positionResolver.x
						a2[occurrenceIndex] = *positionResolver.y
					}
				} else {
					a1[occurrenceIndex] = posX
					a2[occurrenceIndex] = posY
				}
				x1[occurrenceIndex] = iVal.sourceX
				y1[occurrenceIndex] = iVal.sourceY
				x2[occurrenceIndex] = iVal.transmitterY
				y2[occurrenceIndex] = iVal.transmitterY

				occurrenceIndex++
			}
		}

		b := overDeterminedWithTaylor.GetB(
			e, e0,
			a1, a2,
			x1, y1,
			x2, y2,
		)
		A := overDeterminedWithTaylor.GetA(
			e, e0,
			a1, a2,
			x1, y1,
			x2, y2,
		)

		A1 := new(mat.Dense)
		A1.Mul(A.T(), A)

		A1inv := new(mat.Dense)
		err := A1inv.Inverse(A1)
		if err != nil {
			log.Print("Inversion error: ", err.Error())
		}

		Apinv := new(mat.Dense)
		Apinv.Mul(A1inv, A.T())

		X := new(mat.Dense)
		X.Mul(Apinv, b)

		posX = X.At(0, 0)
		posY = X.At(1, 0)
	}

	if math.IsNaN(posX) || posX < 0 || posX > float64(positionResolver.lightBedProperties.RealSizeX) ||
		math.IsNaN(posY) || posX < 0 || posY > float64(positionResolver.lightBedProperties.RealSizeY) {

		positionResolver.x = nil
		positionResolver.y = nil
	} else {
		positionResolver.x = &posX
		positionResolver.y = &posY
	}
}

func (positionResolver *naivePositionResolver) Draw(img image.RGBA, w int, h int) {
	return
	bank := contract.BankId(17) + 8

	positionResolver.biggestExtremeLock.RLock()
	biggestExtreme := positionResolver.biggestExtreme[bank]
	positionResolver.biggestExtremeLock.RUnlock()

	positionResolver.maxKnownValueLock.RLock()
	maxKnownUnitEnergy := positionResolver.maxKnownUnitEnergy[bank]
	positionResolver.maxKnownValueLock.RUnlock()

	if biggestExtreme == nil || maxKnownUnitEnergy == nil {
		return
	}

	allowedBiggerThreshold := biggestExtreme[len(biggestExtreme)-1].value
	l := math.Floor(math.Log10(float64(allowedBiggerThreshold)))
	allowedBiggerThreshold -= uint16(float64(allowedBiggerThreshold) / math.Pow(10, l-1))

	dotSize := int(w / 200)

	sourceX, sourceY := positionResolver.getSourcePosition(bank)

	positionX := int(float64(w)*sourceX) / int(positionResolver.lightBedProperties.RealSizeX)
	positionY := int(float64(h)*sourceY) / int(positionResolver.lightBedProperties.RealSizeY)

	draw.Dot(
		img,
		dotSize,
		positionX,
		positionY,
		color.RGBA64{
			R: 0x0,
			G: 0x0,
			B: 0x0,
			A: 0xffff,
		},
	)

	e := make([]uint16, positionResolver.interestCount)
	e0 := make([]float64, positionResolver.interestCount)
	a1 := make([]float64, positionResolver.interestCount)
	a2 := make([]float64, positionResolver.interestCount)
	x1 := make([]float64, positionResolver.interestCount)
	y1 := make([]float64, positionResolver.interestCount)
	x2 := make([]float64, positionResolver.interestCount)
	y2 := make([]float64, positionResolver.interestCount)

	for i, iVal := range biggestExtreme {
		if iVal.value < allowedBiggerThreshold {
			// continue
		}

		e[i] = iVal.value
		e0[i] = maxKnownUnitEnergy[iVal.x][iVal.y]
		a1[i] = iVal.transmitterX // initial condition in transmitter
		a2[i] = iVal.transmitterY
		x1[i] = iVal.sourceX
		y1[i] = iVal.sourceY
		x2[i] = iVal.transmitterY
		y2[i] = iVal.transmitterY

		positionX = int(float64(w)*iVal.transmitterX) / int(positionResolver.lightBedProperties.RealSizeX)
		positionY = int(float64(h)*iVal.transmitterY) / int(positionResolver.lightBedProperties.RealSizeY)

		draw.Dot(
			img,
			dotSize,
			positionX,
			positionY,
			color.RGBA64{
				R: 0xffff,
				G: 0x0,
				B: iVal.value,
				A: 0xffff,
			},
		)
	}

	b := overDeterminedWithTaylor.GetB(
		e, e0,
		a1, a2,
		x1, y1,
		x2, y2,
	)
	A := overDeterminedWithTaylor.GetA(
		e, e0,
		a1, a2,
		x1, y1,
		x2, y2,
	)

	A1 := new(mat.Dense)
	A1.Mul(A.T(), A)

	A1inv := new(mat.Dense)
	err := A1inv.Inverse(A1)
	if err != nil {
		log.Print("Inversion error: ", err.Error())
	}

	Apinv := new(mat.Dense)
	Apinv.Mul(A1inv, A.T())

	X := new(mat.Dense)
	X.Mul(Apinv, b)

	posX := X.At(0, 0)
	posY := X.At(1, 0)

	positionX = int(float64(w)*posX) / int(positionResolver.lightBedProperties.RealSizeX)
	positionY = int(float64(h)*posY) / int(positionResolver.lightBedProperties.RealSizeY)

	draw.Dot(
		img,
		dotSize*4,
		positionX,
		positionY,
		color.RGBA64{
			R: 0x00ff,
			G: 0x00ff,
			B: 0x00ff,
			A: 0xffff,
		},
	)
}

func (positionResolver *naivePositionResolver) getSourcePosition(bankId contract.BankId) (float64, float64) {
	bankId -= 1 // bank with id 0 is virtual
	row := float64(bankId / 8)
	cel := float64(bankId % 8)

	x := positionResolver.lightBedProperties.IRLedPositionX
	y := positionResolver.lightBedProperties.IRLedPositionY

	x += cel * positionResolver.lightBedProperties.IRLedPositionOffsetX
	y += row * positionResolver.lightBedProperties.IRLedPositionOffsetY

	return x, y
}

func (positionResolver *naivePositionResolver) getTransmitterPosition(x, y uint8) (float64, float64) {
	posX := positionResolver.lightBedProperties.TransistorPositionX
	posY := positionResolver.lightBedProperties.TransistorPositionY

	posX += float64(x) * positionResolver.lightBedProperties.TransistorPositionOffsetX
	posY += float64(y) * positionResolver.lightBedProperties.TransistorPositionOffsetY

	return posX, posY
}

func NewNaivePositionResolver(
	interestCount uint8,
	bankCount uint8,
	overdeterminedIteration uint8,
	lightBedProperties configuration.LightBedProperties,
) contract.PositionResolver {
	maxKnownValue := make(map[contract.BankId]map[uint8]map[uint8]uint16, lightBedProperties.BankCount)
	maxKnownUnitEnergy := make(map[contract.BankId]map[uint8]map[uint8]float64, lightBedProperties.BankCount)

	for i := contract.BankId(0); i < lightBedProperties.BankCount; i++ {
		maxKnownValue[i] = make(map[uint8]map[uint8]uint16, lightBedProperties.Width)
		maxKnownUnitEnergy[i] = make(map[uint8]map[uint8]float64, lightBedProperties.Width)

		for j := uint8(0); j < lightBedProperties.Width; j++ {
			maxKnownValue[i][j] = make(map[uint8]uint16, lightBedProperties.Height)
			maxKnownUnitEnergy[i][j] = make(map[uint8]float64, lightBedProperties.Height)

			for k := uint8(0); k < lightBedProperties.Height; k++ {
				maxKnownValue[i][j][k] = 0
				maxKnownUnitEnergy[i][j][k] = 0
			}
		}
	}

	positionResolver := &naivePositionResolver{
		biggestExtreme:          make(extremeMap),
		maxKnownEnergy:          maxKnownValue,
		maxKnownUnitEnergy:      maxKnownUnitEnergy,
		interestCount:           interestCount,
		bankCount:               bankCount,
		overdeterminedIteration: overdeterminedIteration,
		lightBedProperties:      lightBedProperties,

		biggestExtremeLock: sync.RWMutex{},
		maxKnownValueLock:  sync.RWMutex{},
	}

	return positionResolver
}
