package position_resolver

import (
	"encoding/gob"
	"fmt"
	"gitlab.com/cvut_magister/thesis/viewer/contract"
	"log"
	"os"
	"time"
)

type storingPositionResolver struct {
	path     string
	storeTag *string

	storingChan chan HistoryNode
}

type HistoryNode struct {
	Transmitter [][]*uint16
	UpdatedBank contract.BankId
	Time        time.Time
}

func (positionResolver *storingPositionResolver) Update(transmitter map[contract.BankId][][]uint16, updatedBank contract.BankId) (*float64, *float64) {
	update := make([][]*uint16, 8)

	for x, xVal := range transmitter[updatedBank] {
		if xVal == nil {
			continue
		}

		update[x] = make([]*uint16, 8)

		for y, yVal := range xVal {
			yValCp := yVal
			update[x][y] = &yValCp
		}
	}

	positionResolver.storingChan <- HistoryNode{
		Transmitter: update,
		UpdatedBank: updatedBank,
		Time:        time.Now(),
	}

	return nil, nil
}

func (positionResolver *storingPositionResolver) Store() {
	folder := positionResolver.path
	err := os.Mkdir(folder, 0774)
	if err != nil {
		log.Print("Failed to create directory: ", err.Error())
	}

	if positionResolver.storeTag != nil {
		folder = fmt.Sprintf("%s/%s", folder, *positionResolver.storeTag)
		err := os.Mkdir(folder, 0774)
		if err != nil {
			log.Print("Failed to create directory: ", err.Error())
		}
	}

	tick := time.NewTicker(time.Second)

	i := 0
	nodes := map[int]HistoryNode{}

	for {
		select {
		case node := <-positionResolver.storingChan:
			nodes[i] = node
			i++

		case <-tick.C:

			file, err := os.Create(fmt.Sprintf("%s/%d", folder, time.Now().UnixNano()))
			if err == nil {
				encoder := gob.NewEncoder(file)
				err = encoder.Encode(nodes)
			} else {
				log.Print("Failed to store history node: ", err.Error())
			}

			err = file.Close()
			if err != nil {
				log.Print("Failed to close history node file: ", err.Error())
			}

			nodes = make(map[int]HistoryNode)
			i = 0
		}
	}
}

// This is meta storing resolver
// The purpose is to store nearly raw data measured by light bed, data can be recovered by RestoringServer
func NewStoringPositionResolver(
	path string,
	storeTag *string,
) contract.PositionResolver {
	positionResolver := &storingPositionResolver{
		path:        path,
		storeTag:    storeTag,
		storingChan: make(chan HistoryNode),
	}

	go positionResolver.Store()

	return positionResolver
}
